NPM specific:

update NPM - npm install -g npm@next


update Node - npm install -g node@latest

========================================

Project commands:


//generate a new project
ng new ng6project

//start the server
ng serve -o

//---generate a new component
ng generate component hero-detail

//---generate a new service
ng generate service hero

//install animations
npm install @angular/animations@latest --save